FROM python:3.11

# set working directory
WORKDIR /usr/src/app
COPY install-poetry.py ./
ENV POETRY_HOME=/opt/poetry
RUN python3 install-poetry.py --version 1.2.0 && \
	$POETRY_HOME/bin/poetry --version

# # Install CURL
# RUN apt-get update && \
# 	apt-get install -y --no-install-recommends \
#     curl \
# 	;
# # Download & install poetry
# RUN export POETRY_HOME=/opt/poetry && \
#     curl -sSL https://install.python-poetry.org | python3 - \
# 	;
# Add poetry to path.
ENV PATH=$PATH:/opt/poetry/bin
# add and install requirements
COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --ignore-installed distlib --no-cache-dir -r requirements.txt

#CMD ["cat", "/etc/os-release"]
